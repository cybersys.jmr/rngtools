extends "res://addons/gut/test.gd"


func before_all() -> void:
	randomize()


# Test that randi_range() works
func test_randi_range() -> void:
	assert_eq(0, RNGTools.randi_range(0, 0))
	assert_eq(1, RNGTools.randi_range(1, 1))
	assert_eq(-1, RNGTools.randi_range(-1, -1))

	for i in range(100):
		assert_between(RNGTools.randi_range(10, 20), 9, 19)

	for i in range(100):
		assert_between(RNGTools.randi_range(20, 10), 11, 20)

	for i in range(100):
		assert_between(RNGTools.randi_range(-10, 10), -11, 9)


# Tests that array shuffling results in an array with the same elements but in
# a different order.
func test_shuffle() -> void:
	var array := range(100)
	RNGTools.shuffle(array)
	assert_ne(array, range(100))
	array.sort()
	assert_eq(array, range(100))


# Test that pick() works
func test_pick() -> void:
	var r := range(10)

	var count := [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
	for i in range(1000):
		count[RNGTools.pick(r)] += 1

	# Make sure the distribution is roughly uniform
	for i in range(10):
		assert_almost_eq(count[i], 100, 25)


# Test that pick_many() works
func test_pick_many() -> void:
	var r := range(10)

	var count := [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
	for i in range(1000):
		var result := RNGTools.pick_many(r, 3)
		assert_eq(result.size(), 3)
		for j in result:
			count[j] += 1

	# Make sure the distribution is roughly uniform
	for i in range(10):
		assert_almost_eq(count[i], 300, 50)


# Test that pick_many() works when n is greater than the length of the array
func test_pick_too_many() -> void:
	var r := range(10)
	var p := RNGTools.pick_many(r, 100)
	assert_eq(r, p)


# Test that all methods use the given RNG if they are provided one
func test_given_rng() -> void:
	var rng := RandomNumberGenerator.new()
	rng.seed = hash("Hello, world!")

	var bag := RNGTools.WeightedBag.new()
	bag.weights = {
		A = 1,
		B = 2,
		C = 3
	}

	assert_eq(930045, RNGTools.randi_range(-1000000, 1000000, rng))
	var array := range(10)
	RNGTools.shuffle(array, rng)
	assert_eq([2, 7, 0, 5, 8, 6, 4, 9, 1, 3], array)
	assert_eq(814, RNGTools.pick(range(1000), rng))

	assert_eq("C", RNGTools.pick_weighted(bag, rng))
	assert_eq("A", RNGTools.pick_weighted(bag, rng))
	assert_eq("B", RNGTools.pick_weighted(bag, rng))
	assert_eq("C", RNGTools.pick_weighted(bag, rng))
	assert_eq("C", RNGTools.pick_weighted(bag, rng))

	assert_eq(["A", "C", "E"], RNGTools.pick_many(["A", "B", "C", "D", "E"], 3, rng))


# Test the weighted RNG
func test_pick_weighted() -> void:
	var bag := RNGTools.WeightedBag.new()
	bag.weights = {
		A = 11,
		B = 27,
		C = 13,
		D = 1,
		E = 1,
		F = 1,
		G = 9,
	}

	var sums := {}
	for key in bag.weights.keys():
		sums[key] = 0

	for i in range(10000):
		sums[RNGTools.pick_weighted(bag)] += 1

	for key in bag.weights:
		assert_almost_eq(sums[key] as float, 10000 * (bag.weights[key] as float / bag._sum), 500)
